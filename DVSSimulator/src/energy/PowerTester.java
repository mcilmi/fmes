package energy;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cpu.CPUState;
import cpu.Core;
import cpu.Processor;
import cpu.ProcessorState;

public class PowerTester {
	private static List<ProcessorState> visitedStates = null;
	private static Processor proc1 = null;
	public static void main(String[] args) {

		String coreName = "Core1";
		Core theCore = new Core(coreName);
		EnergyModelOneCore energy = new EnergyModelOneCore(theCore);
		
		Core core = energy.getCore();
		
		//Lets use only the C0 state entering 0 entries and 0 idle time for the other C-states
		CPUState c_zero = core.getState(0);
		c_zero.setNumberOfEntries(10);
		System.out.println("number of entries for C0: " + c_zero.getNumberOfEntries());
		c_zero.setTotTimeDuration(200);
		System.out.println("time for C0: " + c_zero.getTotTimeDuration());
		
		CPUState c_one = core.getState(1);
		c_one.setNumberOfEntries(4);
		System.out.println("number of entries for C1: "+c_one.getNumberOfEntries());
		c_one.setTotTimeDuration(600);
		System.out.println("time for C1: "+c_one.getTotTimeDuration());
		
		CPUState c_two = core.getState(2);

		c_two.setNumberOfEntries(20);
		c_two.setTotTimeDuration(400);
		
		CPUState c_three = core.getState(3);
		c_three.setNumberOfEntries(30);
		c_three.setTotTimeDuration(400);
		
		double power = energy.idleStateSingleCore(100);
		System.out.println("Total power of "+coreName+": "+power+ "mW");
		
		
		//Test of the NEW power setup
		proc1 = new Processor(1e-9);
		System.out.println("-------- Power Calculation using new Setup ------");
		visitedStates = new ArrayList<>();
		double powerInst = 0, powerSum = 0;
		DecimalFormat formatter = new DecimalFormat("0.00");
		for(int i=0;i<proc1.numberOfStates();i++){
			//Set the current state of the processor
			proc1.setCurrentState(i);
			//Add the visited state to the list for later use.
			visitedStates.add(proc1.CurrentState());
			powerInst = proc1.getPower(1);
			//Sum up the power in each visited state
			powerSum += power;
			//Print the power consumption in each state
			System.out.println("Power in " + proc1.CurrentState().getName() + " = " + formatter.format(powerInst) + "W");
		}
		System.out.println("Average power: " + formatter.format(powerSum/(double)visitedStates.size()) + " mW");
		System.out.println("---- End of Power Calculations using new Setup ----");
		System.out.println();
		System.out.println("-------- Select lowest freq ------");
		System.out.println("State:" + selectFreq(0.5).getName());
		System.out.println("Util=1 Select freq = " + selectFreq(1).getFreq());
		System.out.println("Util=1 Select freq = " + selectFreq(0.958).getFreq());
		System.out.println("Util=0.9 Select freq = " + selectFreq(0.9).getFreq());
		System.out.println("Util=0.8 Select freq = " + selectFreq(0.8).getFreq());
		System.out.println("Util=0.7 Select freq = " + selectFreq(0.7).getFreq());
		System.out.println("Util=0.6 Select freq = " + selectFreq(0.6).getFreq());
		System.out.println("Util=0.5 Select freq = " + selectFreq(0.5).getFreq());
		System.out.println("Util=0.3 Select freq = " + selectFreq(0.3).getFreq());
		System.out.println("Util=0.2 Select freq = " + selectFreq(0.2).getFreq());
		System.out.println("Util=0.1 Select freq = " + selectFreq(0.1).getFreq());
	}
	
	public static ProcessorState selectFreq(double util){
		ProcessorState states[] = proc1.getStates();
		ProcessorState state = new ProcessorState();
		
		//Add the frequencies to a list, only to compute the max frequency
		List<Double> freqs = new ArrayList<Double>();
		for(int i=0;i<proc1.numberOfStates();i++){
			freqs.add(states[i].getFreq());
		}
		double max = 0;
        for(int i=0;i<freqs.size();i++){
            if(freqs.get(i) > max)
                max = freqs.get(i);
        }
        //System.out.println("The Max Value is: " + max);
		for(int i=0;i<states.length;i++){
			if(util <= (states[i].getFreq()/max)){
				state = proc1.getState(i);
				
				return state;
			}
		}
		return state;
	}
	

}
