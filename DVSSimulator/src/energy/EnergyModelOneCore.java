package energy;

import cpu.Core;

public class EnergyModelOneCore {
	
	
	String coreName = null; //name of the core
	Core core; //the core
	
	public EnergyModelOneCore(Core theCore){
		core = theCore;
	}

	public Core getCore(){
		return core;
	}
	
	/**
	 * Compute the dynamic power of one core
	 */
	public double idleStateSingleCore(int utilizationFactor){
		
		int numberOfStates = core.getNumberOfStates();
		int t_idle = 0;
		double avg_entry_duration[] = new double[numberOfStates];
		double w[] = new double[numberOfStates];
		double wed[] = new double[numberOfStates];
		double p = 0;		
		double power_core = core.getBetaU() * utilizationFactor + core.getC();
		//calculate the total idle time Ti
		for (int i = 0; i < numberOfStates; i++){
			t_idle += core.getState(i).getTotTimeDuration();
		}
		System.out.println("t_idle: " + t_idle);
		
		for (int i = 0; i < numberOfStates; i++){
			
			if(core.getState(i).getNumberOfEntries() != 0){
				
				//average entry duration for idle state Ci
				avg_entry_duration[i] = core.getState(i).getTotTimeDuration() / core.getState(i).getNumberOfEntries();
				
				//time the CPU stay at the state Ci over the whole idle period
				w[i] = core.getState(i).getTotTimeDuration() / (double)t_idle;
				
				//weighted average entry duration WEDCi
				wed[i] = w[i] * avg_entry_duration[i];
				System.out.println("wed["+i+"]: " +wed[i]);
				System.out.println(power_core);
			}
			else{
				//wed[i] = 0;
				System.out.println("in else");
				System.out.println("wed["+i+"]: " +wed[i]);
			}
			
			//power of the core in the idle states
			power_core =  power_core + core.getState(i).getCoefficient() * wed[i];
		}
		
		//System.out.println("The core " +coreName+ " consume a power of " +power_core+ "mW");
		return power_core;
		
	}
	
}
