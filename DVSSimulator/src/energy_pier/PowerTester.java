package energy_pier;

import cpu_pier.CPUState;
import cpu_pier.Core;

public class PowerTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String coreName = "Core1";
		Core theCore = new Core(coreName);
		EnergyModelOneCore energy = new EnergyModelOneCore(theCore);
		
		Core core = energy.getCore();
		
		//lets use only the C0 state entering 0 entries and 0 idle time for the other C-states
		CPUState c_zero = core.getState(0);
		c_zero.setNumberOfEntries(2);
		System.out.println("number of entries for C0: "+c_zero.getNumberOfEntries());
		c_zero.setTotTimeDuration(100);
		System.out.println("time for C0: "+c_zero.getTotTimeDuration());
		
		CPUState c_one = core.getState(1);
		c_one.setNumberOfEntries(4);
		System.out.println("number of entries for C1: "+c_one.getNumberOfEntries());
		c_one.setTotTimeDuration(600);
		System.out.println("time for C1: "+c_one.getTotTimeDuration());
		
		CPUState c_two = core.getState(2);
		c_two.setNumberOfEntries(2);
		c_two.setTotTimeDuration(100);
		
		CPUState c_three = core.getState(3);
		c_three.setNumberOfEntries(2);
		c_three.setTotTimeDuration(100);
		
		double power = energy.idleStateSingleCore(100);
//		System.out.println("The power of the core "+coreName+" is "+power+ "mW");
	}

}
