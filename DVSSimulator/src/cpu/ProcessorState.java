package cpu;

public class ProcessorState {
	private double voltage = 0;
	private double frequency = 0;
	public static double currentUtil = 0;
	String name = null;
	
	/**
	 * Instantiate freq and voltage
	 * @param v voltage in Volt
	 * @param freq frequency in Hz
	 */
	public ProcessorState(double v, double freq, String name){
		setVoltage(v);
		setFreq(freq);
		setName(name);
	}
	public ProcessorState(){
		
	}
	
	/**
	 * Set the frequency
	 * @param freq frequency in Hz
	 */
	public void setFreq(double freq){
		this.frequency = freq;
	}
	/**
	 * 
	 * @return frequency of this state in Hz.
	 */
	public double getFreq(){
		return this.frequency;
	}
	
	/**
	 *
	 * @return voltage in this state in Volt.
	 */
	public double getVoltage(){
		return this.voltage;
	}
	
	/**
	 * Set the voltage in Volt for the current
	 * state
	 * @param v
	 */
	public void setVoltage(double v){
		this.voltage = v;
	}
	
	/**
	 * Set the name for the current state
	 * @param name
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * 
	 * @return the name of the current state as String
	 */
	public String getName(){
		return this.name;
	}
	
	/*
	 * Set the current util-factor of the current state
	 * @param ut utilization factor (0-1)
	 */
	public void setCurrentUtil(double ut){
		currentUtil = ut;
	}
	/* 
	 * @return current util of this state 
	 */
	public double getCurrentUtil(){
		return currentUtil;
	}
	
}
