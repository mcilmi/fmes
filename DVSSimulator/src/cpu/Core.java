package cpu;

public class Core {
	
	private double betaU = 1; //coefficient for the utilization
	private double c = 1; //constant dependent on the core
	private int numberOfStates = 4;
	CPUState[] states = new CPUState[numberOfStates];
	
	public Core(String coreName){
		states[0] = new CPUState("C0", 0, 4000, 0, 1000, 0.6);
		states[1] = new CPUState("C1", 200, 3200, 150, 800, 0.4);
		states[2] = new CPUState("C2", 550, 2760, 400, 400, 0.2);
		states[3] = new CPUState("C3", 1100, 1230, 600, 100, 0.1);
	}
	
	public CPUState getState(int number){
		return states[number];
	}
	
	public double getBetaU(){
		return betaU;
	}
	
	public double getC(){
		return c;
	}
	public int getNumberOfStates(){
		return numberOfStates;
	}
}
