package cpu;

public class Processor {
	private static ProcessorState currentState = null;
	private static ProcessorState states[] = new ProcessorState[4];
	private double capacitance = 0; //1nF
	
	/**
	 * 
	 * @param c the capacitance of the processor -
	 * in Farad(usually 1nF(1e-9 Farad))
	 */
	public Processor(double c) {
		states[0] = new ProcessorState(1.28, 1700e6,"C0"); //Tick=1; MAX 	Perfomance= 1
		states[1] = new ProcessorState(1.08, 1200e6,"C1"); //Tick=2  HIGH 	Performance=.7058
		states[2] = new ProcessorState(0.7, 700e6,"C2");  //Tick=3  LOW		Performance=.412	
		states[3] = new ProcessorState(0.2, 200e6,"C3");  //Tick=4  MIN		Performance=.166
		
		this.capacitance = c;
	}
	
	public Processor() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @param currentS
	 */
	public void setCurrentState(int index){
		Processor.currentState = states[index];
	}
	
	/**
	 * 
	 * @return
	 */
	public ProcessorState CurrentState(){
		return Processor.currentState;
	}
	
	private double getCapacitance(){
		return this.capacitance;
	}
	
	/**
	 * Calculate the power
	 * @return power in Watts
	 */
	public double getPower(double util){
		//DecimalFormat formatter = new DecimalFormat("0.00");
		double power = getCapacitance() * CurrentState().getFreq() * Math.sqrt(CurrentState().getVoltage()) * util;
		return power;
	}
	/**
	 * 
	 * @return the number of states
	 */
	public int numberOfStates(){
		return states.length;
	}
	public ProcessorState getState(int index){
		return states[index];
	}
	
	public ProcessorState[] getStates(){
		
		return states;
	}
}
