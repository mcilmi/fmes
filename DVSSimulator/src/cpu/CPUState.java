package cpu;

/**
 * It is expected to have at least the states: C0, C1, C2 and C3 plus the full-operative state
 * The user is suggested to use a vector to manipulate these C-states
 *
 */
public class CPUState {

	private String name;
	private int exit_latency; //how long it takes to get back to a fully functional state
	private int power_usage; //the amount of power consumed by the CPU when it is in this state
	private int target_residency; /*the minimum amount of time the processor should spend 
									   in this state to make the transition worth the effort*/
	private double frequency; //operation frequency of the state
	private int totTime; //total time duration that a CPU core stays in this idle state
	private int entries; //number of entries for this idle state
	
	double betaC = 0; //coefficient for WEDCi
	
	/**
	 * @param name_state name of the state
	 * @param latency how long it takes to get back to a fully functional state
	 * @param power the amount of power consumed by the CPU when it is in this state
	 * @param residency the minimum amount of time the processor should spend 
	 * 					in this state to make the transition worth the effort
	 * @param freq operation frequency of the state
	 */
	public CPUState(String name_state, int latency, int power, int residency, double freq, double betaCoeff){
		name = name_state;
		exit_latency = latency;
		power_usage = power;
		target_residency = residency;
		frequency = freq;
		totTime = 0;
		entries = 0;
		betaC = betaCoeff;
	}
	
	public String getName(){
		return name;
	}
	
	public int getLatency(){
		return exit_latency;
	}
	
	public int getPower(){
		return power_usage;
	}
	
	public int getResidency(){
		return target_residency;
	}
	
	public double getFrequency(){
		return frequency;
	}
	
	public int getNumberOfEntries(){
		return entries;
	}
	public void setNumberOfEntries(int number){
		entries = number;
	}
	
	public int getTotTimeDuration(){
		return totTime;
	}
	public void setTotTimeDuration(int time){
		totTime = time;
	}
	
	public double getCoefficient(){
		return betaC;
	}
}
