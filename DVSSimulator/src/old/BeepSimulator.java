package old;

class Beep extends Event {

    Beep(double time) {
        this.time = time;
    }

    @Override
    void execute(AbstractSimulator simulator) {
        System.out.println("The time is " + time);
    }
}

class BeepSimulator extends Simulator {

    public static void main(String[] args) {
        new BeepSimulator().start();
    }

    void start() {
        events = new ListQueue();
        insert(new Beep(4.0));
        insert(new Beep(1.0));
        insert(new Beep(1.5));
        insert(new Beep(2.0));       
        
        doAllEvents();
    }
}
