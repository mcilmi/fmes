package simulator;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public interface Engine {
	Task getRunningTask();
	List<Task> getResettedTasks();
	void setState(int state);
	int getState();
}
