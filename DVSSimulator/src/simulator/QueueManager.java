package simulator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import cpu.Processor;
import cpu.ProcessorState;

public class QueueManager implements Runnable{
	private List<Task> queue;
	private StopWatch timer;
	private BlockingQueue<Task> msgEngineqQueue;
	private volatile boolean taskAddedFlag = false;
	private Engine cpuEngine;
	private int cpuState;
	private int runningTime;
	Processor proc1 = new Processor(1e-9);
	private List<CalculateEnergyUsage> TasksExecuted;
	public QueueManager(BlockingQueue<Task> q,Engine engine, StopWatch timer, int runningTime){
		this.queue = new ArrayList<Task>();
		TasksExecuted = new ArrayList<CalculateEnergyUsage>();
		this.timer = timer;
		this.msgEngineqQueue = q;
		this.cpuEngine = engine;
		this.runningTime = runningTime;
	}
	
	public void Add(Task task){
		
		taskAddedFlag = true;
		queue.add(task);
	}
	
	public double getActualTime(){
		return timer.elapsedTime();
	}
	
	public ProcessorState selectFreq(List<Task> Tasks){
		double util = 0;
		
		for(int i = 0; i < Tasks.size(); i++){
			if(Tasks.get(i).getActualCase()> -1){
				System.out.println("selectFreq() for:");
				util += ((double)Tasks.get(i).getActualCase()/(double)Tasks.get(i).getPeriod());
			}
			else{
				System.out.println("selectFreq() in else");
				util += ((double)Tasks.get(i).getWorstcase()/(double)Tasks.get(i).getPeriod());
			}
		}
		System.out.println("Qmanager: Util Factor: " + util);
		//Add the frequencies to a list, only to compute the max frequency
		ProcessorState states[] = proc1.getStates();
		ProcessorState state = new ProcessorState();
		List<Double> freqs = new ArrayList<Double>();
		for(int i=0;i<proc1.numberOfStates();i++){
			freqs.add(states[i].getFreq());
		}
		if(util > 0.5){
			proc1.setCurrentState(3);//Max state
			proc1.CurrentState().setCurrentUtil(util);
			return proc1.getState(3);
		}			
		double max = 0;
        for(int i=0;i<freqs.size();i++){
            if(freqs.get(i) > max)
                max = freqs.get(i);
        }
        //System.out.println("The Max Value is: " + max);
		for(int i=0;i<states.length;i++){
			if(util <= (states[i].getFreq()/max)){
				state = proc1.getState(i);
				proc1.setCurrentState(i);
				System.out.println("Qmanager: min freg found: " + state.getName());
				return state;
			}
		}
		System.out.println("Qmanager: min freg found not found ");
		proc1.setCurrentState(3);//Max state
		proc1.CurrentState().setCurrentUtil(util);
		return proc1.getState(3);
	}
	
	@Override
	public void run() {
		taskAddedFlag = false;
		//cpuState = ProcessorState.MAX;
		//cpuEngine.setState(ProcessorState.MAX);
		ProcessorState procState = new ProcessorState();
		// set while(runningtime>0) s� der kommer en afslutning
		while(true){
			System.out.println("Qmanager: Running algorithm and queue size: " + queue.size() + " time: " + timer.elapsedTime());
			
			// find out if we can lower or have to increase the frequency of the cpu core 
			// this is done by checking if all the tasks can be run in time
//			for(Task task:queue){
//				System.out.println(task.getName() + " Deadline: " + task.getDeadline() + " Worstcase:" + task.getWorstcase()*(cpuState/1000));
//				System.out.println(task.getName() + " Period: " + task.getPeriod() + " ActualCase:" + task.getActualCase()*(cpuState/1000));
//			}
//			procState = selectFreq(queue);
//			switch( procState.getName()){
//			case "C0": 
//				cpuState = ProcessorState.MIN;
//				cpuEngine.setState(ProcessorState.MIN);
//				System.out.println("Qmanager: Switching State to MIN");
//				break;
//			case "C1":
//				cpuState = ProcessorState.LOW;
//				cpuEngine.setState(ProcessorState.LOW);
//				System.out.println("Qmanager: Switching State to LOW");
//				break;
//			case "C2":
//				cpuState = ProcessorState.HIGH;
//				cpuEngine.setState(ProcessorState.HIGH);
//				System.out.println("Qmanager: Switching State to HIGH");
//				break;
//			case "C3":
//				cpuState = ProcessorState.MAX;
//				cpuEngine.setState(ProcessorState.MAX);
//				System.out.println("Qmanager: Switching State to MAX");
//				break;
//			default: 
//				cpuState = ProcessorState.MAX;
//				cpuEngine.setState(ProcessorState.MAX);
//				System.out.println("Qmanager: Switching State to DEFAULT MAX");
//			}
			
			
			// base case

			Task retTask = FindFirstTask();
			
			
			// compare the chosen task with all the tasks left to find the task that is most pressing
			// reset any tasks that can't be completed because of the deadline
			if(retTask != null){
				
				retTask = FindMostUrgentTask(retTask);
				
				//if the task to run is the same that engine is running then just remove it form the queue again
				// else get the actual task again, and then send the new task to run.
				if(cpuEngine.getRunningTask() == null || !retTask.getName().equals(cpuEngine.getRunningTask())){
					try {
						System.out.println("Qmanager: Pushing Task: " + retTask.getName() + " time: " + timer.elapsedTime());
						msgEngineqQueue.put(retTask);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				System.out.println("Qmanager: Removing Task: " + retTask.getName()+ " Because Added to Queue" + " time: " + timer.elapsedTime());
				queue.remove(retTask);					// check if this removes the right task
			}
			try {
				System.out.println("Qmanager: sleeping 200 miliseconds to give runengine time to set running task" + " time: " + timer.elapsedTime());
				Thread.sleep(200);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			// test if task is finished or if the  check for null
			while(cpuEngine.getRunningTask() != null){
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Task testtask = FindFirstTask();
				
				// if new task has been added or running task is not the most pressing task
				// then add the currently running task to the queue and set it to be the running task
				if(cpuEngine.getRunningTask() != null && testtask != null){
					if(taskAddedFlag == true || !(FindMostUrgentTask(testtask).getName().equals(cpuEngine.getRunningTask().getName()))){
						System.out.println("Qmanager: either new task added or new most urgent task" + " time: " + timer.elapsedTime());
						Task temp = cpuEngine.getRunningTask();
						TasksExecuted.add(new CalculateEnergyUsage(temp.getName(),temp.getTicksComputed(),(cpuState + ""),0));
						temp.setTicksComputed(0);
						queue.add(temp);
						taskAddedFlag = false;
						try {
							msgEngineqQueue.put(new Task("block",0,0,0));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						// add the information about which task, what state and for how long it ran in engine.
						break;
					}
				}
			}
			//add finished periodic tasks
			System.out.println("Qmanager: added completed and resetted tasks to queue again " + "time: " + timer.elapsedTime());
			List<Task> TempList = cpuEngine.getResettedTasks();
			boolean taskalreadyAdded = false;
			for(int i = 0; i < TempList.size(); i++){
				for(int j = 0; j < queue.size(); j++){
					if(TempList.get(i).getName().equals(queue.get(j).getName())){
						taskalreadyAdded = true;
					}
				}
				if(!taskalreadyAdded)
				{
					TasksExecuted.add(new CalculateEnergyUsage(TempList.get(i).getName(),TempList.get(i).getTicksComputed(),(cpuState + ""), 0));
					TempList.get(i).setTicksComputed(0);
					queue.add(TempList.get(i));
				}
			}
			//runningTime--;
		}
		//calcualte energy used System.out.println("used energy and etc.: " );
		
	}
	// if task is inside it's timetorun, meaning it's within it's new period and it hasn't been run then choose it.
	// choose one task that can be performed in the allowed time
	// reset any tasks that can't be completed because of the deadline
	public Task FindFirstTask(){
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getTimetorun() <= getActualTime()){
				if(queue.get(i).getDeadline() < (getActualTime()+queue.get(i).getTicksleft()* (cpuState/1000))){
					System.out.println("Qmanager: Unable to complete Task in Time resetting : " + queue.get(i).getName() + " first loop deadline: "
							+queue.get(i).getDeadline() + " Actual Time + runtaskTime: " + getActualTime()+" " + (queue.get(i).getTicksleft()*(cpuState/1000)));
					queue.get(i).resetPeriod();
				} else {
					//System.out.println("Qmanager: adding task to rettask: " + queue.get(i).getName() + " first loop");
					return queue.get(i);
				}
			}
		}
		return null;
	}
	public Task FindMostUrgentTask(Task retTask){
		for(int i = 1; i < queue.size(); i++){
			Task tempTask = queue.get(i);
			if(tempTask.getTimetorun() <= getActualTime()){
				if(tempTask.getDeadline() < (getActualTime()+tempTask.getTicksleft()* (cpuState/1000))){
					System.out.println("Qmanager: Unable to complete Task in Time resetting: " + queue.get(i).getName() + " second loop deadline: " 
				+ tempTask.getDeadline() + " Actual Time + runtaskTime: " + getActualTime()+" " + (tempTask.getTicksleft()* (cpuState/1000)));
					queue.get(i).resetPeriod();
					continue;
				}
				if(tempTask.getDeadline()- (tempTask.getTicksleft()* (cpuState/1000)) < retTask.getDeadline() - (retTask.getTicksleft()* (cpuState/1000))){
					retTask = tempTask;
				} 
			}
		}
		Task runTask = cpuEngine.getRunningTask();
		
		if(runTask != null){
			if(retTask.getDeadline()- (retTask.getTicksleft()* (cpuState/1000)) > runTask.getDeadline() - (runTask.getTicksleft()* (cpuState/1000))){
				return runTask;
			}
		}
		return retTask;
	}
}
