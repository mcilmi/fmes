package simulator;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

import cpu.Processor;
import cpu.ProcessorState;
import cpu_pier.Core;
import energy_pier.EnergyModelOneCore;

public class CPU{
	private volatile List<Task> queue;
	private StopWatch timer;
	private int cpuState;
	Task runTask;
	boolean taskAdded;
	int runTime;
	private List<CalculateEnergyUsage> TasksExecuted;
	double utilFactor;
	String coreName = "Core1";
	Core theCore = new Core(coreName);
	EnergyModelOneCore energy = new EnergyModelOneCore(theCore);
	Processor proc1 = new Processor(1.1e-9);
	public CPU(StopWatch timer, int runTime){
		utilFactor = 0;
		this.runTime = runTime;
		taskAdded = false;
		TasksExecuted = new ArrayList<CalculateEnergyUsage>();
		this.queue = new ArrayList<Task>();
		this.timer = timer;
		this.cpuState = State.C0;
	}
	public void Add(Task task){
		taskAdded = true;
		queue.add(task);
	}
	
	public double getActualTime(){
		return timer.elapsedTime();
	}
	boolean temp =false;
	boolean dvsFlag=true;
	public void run() {
		if(dvsFlag==false){
			SetUtilityWithoutDVS();
		}		
		try {
			if(dvsFlag==true){
				if(!SelectFrequency()){
					System.out.println("CPU: Unable to Run the task in time. CPU state set to C0");
				}
			}
			 
			taskAdded = false;
			while(!TimeOut()){
				// find the task to run
			    runTask = FindTask();
				
				while(runTask != null){
					if(taskAdded){
						System.out.println("CPU: new task added");
						if(dvsFlag==true){
							if(!SelectFrequency()){
								System.out.println("CPU: Unable to Run the task in time. CPU state set to C0");
							}
						}
						
						taskAdded = false;
					}
					// if it's first time the task is run, then calculate how long it should take, between best and worst case 
					if (runTask.getActualCase() == -1) {
						runTask.setActualCase(randInt(runTask.getBestcase(),runTask.getWorstcase()));
						runTask.setTicksleft(runTask.getActualCase());
						System.out.println("CPU: ActualCase set for: " + runTask.getName() + " set to: " + runTask.getActualCase());
						if(dvsFlag==true){
							if(!SelectFrequency()){
								System.out.println("CPU: Unable to Run the task in time. CPU state set to C0");
							}
						}
						
					}
					System.out.println("CPU: Running task = "+runTask.getName()+" with seconds left: "+((double)runTask.getTicksleft()*getState()/10)+"s"
					+ " and deadline " + runTask.getDeadline() + " acutal time: " + ((double)getActualTime()/10)+"s"+" Performance: "
							+ printPerformance(getState()));
					
					Tick();
					// after 1 tick is done 1 is added to ticks computed. this is used to calculate energy usage
					runTask.setTicksComputed(runTask.getTicksComputed()+1);
					//if more than one ticks remain then decrement the ticksleft by 1 and continue
					if(runTask.getTicksleft()>1){
						runTask.setTicksleft(runTask.getTicksleft()-1);
					} else {
						System.out.println("CPU: Running task = "+runTask.getName()+" with ticks reached 0" + " acutal time: " + ((double)getActualTime()/10) +"s"+" Performance: "
					+ printPerformance(getState()));
						// if task is done running then add the run task to tasksexecuted, which is used to calculate energyusage
						TasksExecuted.add(new CalculateEnergyUsage(runTask.getName(),runTask.getTicksComputed(),(cpuState + ""), utilFactor));
						// then reset the tasks period.
						runTask.resetPeriod();
						// and set it  to null;
						runTask = null;
						break;
					}
					Task testTask = FindTask();
					if(testTask != null && !(testTask.getName().equals(runTask.getName()))){
						System.out.println("CPU: Running task = "+runTask.getName()+" Interrupted by: "+testTask.getName());
						TasksExecuted.add(new CalculateEnergyUsage(runTask.getName(),runTask.getTicksComputed(),(cpuState + ""), utilFactor));
						runTask = testTask;
					}
					if(TimeOut()){
						break;
					}
				}
				//Thread.sleep(50); // should maybe be removed, only added to prevent algorithm from being run very many times in a row...
			}				
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("CPU: Done Running Calculating Engery Usage");
		CalculateTasksAndState(TasksExecuted);
		calculateDynamicPower();
		//calculateS();
	}
	public boolean TimeOut(){
		if(getActualTime()<runTime){
			return false;
		} else {
			return true;
		}
	}
	public void calculateS(){
		int durationc0 = 0;
		for(int i = 0; i< TasksExecuted.size(); i++)
		{
				durationc0 += TasksExecuted.get(i).duration;
		}

		System.out.println("DURATION SAIGNOADG: " + durationc0);
	}

	public void SetUtilityWithoutDVS(){
		double utilC0 = 0;
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getActualCase()> -1){
				utilC0 += ((double)queue.get(i).getActualCase()/(double)queue.get(i).getPeriod());
			}
			else{
				utilC0 += ((double)queue.get(i).getWorstcase()/(double)queue.get(i).getPeriod());
			}
		}
		utilFactor = utilC0;
	}
	public boolean SelectFrequency(){
		double utilC0 = 0;
		double utilC1 = 0;
		double utilC2 = 0;
		double utilC3 = 0;
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getActualCase()> -1){
				utilC0 += ((double)queue.get(i).getActualCase()/(double)queue.get(i).getPeriod());
			}
			else{
				utilC0 += ((double)queue.get(i).getWorstcase()/(double)queue.get(i).getPeriod());
			}
		}
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getActualCase()> -1){
				utilC1 += ((double)queue.get(i).getActualCase()*2 /(double)queue.get(i).getPeriod());
			}
			else{
				utilC1 += ((double)queue.get(i).getWorstcase()*2 /(double)queue.get(i).getPeriod());
			}
		}
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getActualCase()> -1){
				utilC2 += ((double)queue.get(i).getActualCase()*3 /(double)queue.get(i).getPeriod());
			}
			else{
				utilC2 += ((double)queue.get(i).getWorstcase()*3 /(double)queue.get(i).getPeriod());
			}
		}
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getActualCase()> -1){
				utilC3 += ((double)queue.get(i).getActualCase()*4 /(double)queue.get(i).getPeriod());
			}
			else{
				utilC3 += ((double)queue.get(i).getWorstcase()*4 /(double)queue.get(i).getPeriod());
			}
		}
		if(utilC3 < 1){
			System.out.println("CPU:  CPU state set to C3");
			cpuState = State.C3;
			utilFactor = utilC3;
			return true;
		}
		else if(utilC2 < 1){
			System.out.println("CPU:  CPU state set to C2");
			cpuState = State.C2;
			utilFactor = utilC2;
			return true;
		}
		else if(utilC1 < 1){
			System.out.println("CPU:  CPU state set to C1");
			cpuState = State.C1;
			utilFactor = utilC1;
			return true;
		} 
		else if (utilC0 < 1){
			System.out.println("CPU:  CPU state set to C0");
			cpuState = State.C0;
			utilFactor = utilC0;
			return true;
		}
		cpuState = State.C0;
		utilFactor = utilC0;
		return false;
		
	}
	
	public void CalculateTasksAndState(List<CalculateEnergyUsage> energyQueue){
		double durationc0 = 0;
		double durationc1 = 0;
		double durationc2 = 0;
		double durationc3 = 0;
		int entriesc0 = 0;
		int entriesc1 = 0;
		int entriesc2 = 0;
		int entriesc3 = 0;
		double averageutilfactor = 0;
		for(int i = 0; i< energyQueue.size(); i++)
		{
			if(energyQueue.get(i).state.equals("1")){
				durationc0 += (double)energyQueue.get(i).duration/10;
				entriesc0 += 1;
			}
			else if(energyQueue.get(i).state.equals( "2")){
				durationc1 += (double)energyQueue.get(i).duration/10;
				entriesc1 += 1;
			}
			else if(energyQueue.get(i).state.equals("3")){
				durationc2 += (double)energyQueue.get(i).duration/10;
				entriesc2 += 1;
			}
			else if(energyQueue.get(i).state.equals("4")){
				durationc3 += (double)energyQueue.get(i).duration/10;
				entriesc3 += 1;
			}
			averageutilfactor += energyQueue.get(i).util * (double)energyQueue.get(i).duration/10;
		}
		
		if(0 < durationc3 + durationc2 + durationc1 + durationc0){		
			averageutilfactor = (averageutilfactor)/(durationc3 + durationc2 + durationc1 + durationc0);
		}
		int avg = (int)(averageutilfactor*100);
		System.out.println("CPU: Calculating Energy: C0 duration: " +durationc0 +" and entries: " + entriesc0 + 
				" C1 duration: " +durationc1 +" and entries: " + entriesc1 +
				" C2 duration: " +durationc2 +" and entries: " + entriesc2 +
				" C3 duration: " +durationc3 +" and entries: " + entriesc3 + " average util factor: " + averageutilfactor + "    " + avg );
		
		cpu_pier.CPUState C0 = theCore.getState(0);
		C0.setTotTimeDuration(durationc0);
		C0.setNumberOfEntries(entriesc0);
		cpu_pier.CPUState C1 = theCore.getState(1);
		C1.setTotTimeDuration(durationc1);
		C1.setNumberOfEntries(entriesc1);
		cpu_pier.CPUState C2 = theCore.getState(2);
		C2.setTotTimeDuration(durationc2);
		C2.setNumberOfEntries(entriesc2);
		cpu_pier.CPUState C3 = theCore.getState(3);
		C3.setTotTimeDuration(durationc3);
		C3.setNumberOfEntries(entriesc3);
		energy.idleStateSingleCore(avg);
		
	}
	public void calculateDynamicPower(){
		//Test of the NEW power setup
		double power=0, powerSum=0,avgPower=0;
		double energy=0;
		double totalDuration = 0;
		DecimalFormat formatter = new DecimalFormat("0.000");
		int stateIndex = 0;
		String state = null;
		System.out.println("-------- Dynamic Power calculation using Simple Power Model ------");
		if(dvsFlag==true){
			System.out.println("DVS enabled!");
		}else{
			System.out.println("DVS disabled!");
		}
		for(int i=0;i<TasksExecuted.size();i++){
			
			state = TasksExecuted.get(i).state;
			switch(state){
			case "1":
				 stateIndex = 0;//C0
				 break;
			case "2":
				 stateIndex = 1;//C1
				 break;
			case "3":
				 stateIndex = 2;//C2
				 break;
			case "4":
				 stateIndex = 3;//C3
				 break;
			}
			
			//Set the current state to calculate the correct power(using freq and volt)			
			proc1.setCurrentState(stateIndex);
			
			//Calculate Power in Watt for the current task, state and utilization.
			//power = proc1.getPower(TasksExecuted.get(i).util);
			power = proc1.getPower(1);
			powerSum += power;//Sum power, to calculate average Power and Energy usage
			
			//Sum the duration
			totalDuration += ((double)TasksExecuted.get(i).duration/10);
			
			//Calculate the energy in Joule for this task in this state and the duration
			energy = power * (double)TasksExecuted.get(i).duration/10;
			System.out.println(TasksExecuted.get(i).taskName + " State:" + proc1.CurrentState().getName() 
					+ "\tDuration:" + (double)TasksExecuted.get(i).duration/10 +"s"
					+ "\tUtil:" + formatter.format(TasksExecuted.get(i).util) 
					+ "\tPower:" + formatter.format(power)+"W" 
					+ "\tEnergy:" + formatter.format(energy)+ "J");			
		}
		avgPower = powerSum/(double)TasksExecuted.size();
		if(dvsFlag==true){
			System.out.println("DVS enabled!");
		}else{
			System.out.println("DVS disabled!");
		}
		System.out.println("Avarage Power: " + formatter.format(avgPower)+ "W");
		System.out.println("Total duration: " + formatter.format(totalDuration) + "s");
		System.out.println("Total Energy usage: " + formatter.format(avgPower*(totalDuration)) + "J");
		System.out.println("Total Simulation time: " + formatter.format(((double)getActualTime()/10)) + "s");
								
			
			
	}
	
	public double printPerformance(int state){			
		double performance = 0;
		int stateIndex = -1;
		switch(state){
		case 1:
			 stateIndex = 0;//C0
			 break;
		case 2:
			 stateIndex = 1;//C1
			 break;
		case 3:
			 stateIndex = 2;//C2
			 break;
		case 4:
			 stateIndex = 3;//C3
			 break;
		}
		//Set the current state to calculate the correct power(using freq and volt)			
		proc1.setCurrentState(stateIndex);
		performance = proc1.CurrentState().getFreq()/proc1.getState(0).getFreq();
		return performance;
	}
	
	// if task is inside it's timetorun, meaning it's within it's new period and it hasn't been run then choose it.
	// choose one task that can be performed in the allowed time
	// reset any tasks that can't be completed because of the deadline
	public Task FindTask(){
		for(int i = 0; i < queue.size(); i++){
			if(queue.get(i).getTimetorun() <= getActualTime()){
				if(queue.get(i).getDeadline() < (getActualTime()+queue.get(i).getTicksleft()* (cpuState))){
					System.out.println("CPU: Unable to complete Task in Time resetting : " + queue.get(i).getName() + " FindFirstTask deadline: "
							+queue.get(i).getDeadline() + " Actual Time + runtaskTime: " + getActualTime()+" " + (queue.get(i).getTicksleft()*(cpuState)));
					queue.get(i).resetPeriod();
				} else {
					//System.out.println("CPU: adding task to rettask: " + queue.get(i).getName() + " first loop");
					return FindMostUrgentTask(queue.get(i));
				}
			}
		}
		return null;
	}
	public Task FindMostUrgentTask(Task retTask){
		for(int i = 1; i < queue.size(); i++){
			Task tempTask = queue.get(i);
			if(tempTask.getTimetorun() <= getActualTime()){
				if(tempTask.getDeadline() < (getActualTime()+tempTask.getTicksleft()* (cpuState))){
					System.out.println("CPU: Unable to complete Task in Time resetting: " + queue.get(i).getName() + " FindMostUrgentTask deadline: " 
				+ tempTask.getDeadline() + " Actual Time + runtaskTime: " + getActualTime()+" " + (tempTask.getTicksleft()* (cpuState)));
					queue.get(i).resetPeriod();
					i--;
					continue;
				}
				if(tempTask.getDeadline()  < retTask.getDeadline()){
					retTask = tempTask;
				} 
			}
		}
		//compare with the currently running task, if it is less urgent then the currently running task, then no reason to interrupt
		if(runTask != null){
			if(retTask.getDeadline() >= runTask.getDeadline()){
				return runTask;
			}
			System.out.println("CPU comparing deadlines: " + runTask.getName() + " " + runTask.getDeadline()
			+ " " +  retTask.getName() + " " + retTask.getDeadline()); 
		}
		return retTask;
	}
	public void Tick() throws InterruptedException{
		Thread.sleep(getState()*100);
	}
	
	public int getState() {
		return cpuState;
	}
	
	public void setState(int state){
		cpuState = state;
	}
	public static int randInt(int min, int max) {

	    // NOTE: This will (intentionally) not run as written so that folks
	    // copy-pasting have to think about how to initialize their
	    // Random instance.  Initialization of the Random instance is outside
	    // the main scope of the question, but some decent options are to have
	    // a field that is initialized once and then re-used as needed or to
	    // use ThreadLocalRandom (if using at least Java 1.7).
	    Random rand = new Random();
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    return rand.nextInt((max - min) + 1) + min;
	}
}
