package simulator;

public class DVS {
	
	public final static int FREQUENCY=10000;
	public final static int TICK_VALUE=1000;
	public final static int TICK_VALUE_SEC=1;
	public final static int QUEUE_SIZE=1000;

	public static void main(String[] args) {
		StopWatch timer = new StopWatch();

        CPU cpu = new CPU(timer, 2400);
        //state 1 dvs.
       /* cpu.Add(new Task("task1", 4,4,100));
        cpu.Add(new Task("task2", 6,6,150));
        cpu.Add(new Task("task3", 8,8,100));
        cpu.Add(new Task("task4", 7,7,100));
        cpu.Add(new Task("task5", 3,3,100));
        cpu.Add(new Task("task6", 2,2,120));
        cpu.Add(new Task("task7", 8,8,140));
        cpu.Add(new Task("task8", 5,5,100));
        cpu.Add(new Task("task9", 7,7,200));
        cpu.Add(new Task("task10", 9,9,150));*/

        //state 2 dvs
        /*cpu.Add(new Task("task1", 4,4,200));
        cpu.Add(new Task("task2", 6,6,150));
        cpu.Add(new Task("task3", 8,8,200));
        cpu.Add(new Task("task4", 7,7,200));
        cpu.Add(new Task("task5", 3,3,200));
        cpu.Add(new Task("task6", 2,2,220));
        cpu.Add(new Task("task7", 8,8,240));
        cpu.Add(new Task("task8", 5,5,200));
        cpu.Add(new Task("task9", 7,7,200));
        cpu.Add(new Task("task10", 9,9,250));*/
        
      //state 3 dvs
       /* cpu.Add(new Task("task1", 4,4,300));
        cpu.Add(new Task("task2", 6,6,250));
        cpu.Add(new Task("task3", 8,8,300));
        cpu.Add(new Task("task4", 7,7,300));
        cpu.Add(new Task("task5", 3,3,300));
        cpu.Add(new Task("task6", 2,2,320));
        cpu.Add(new Task("task7", 8,8,340));
        cpu.Add(new Task("task8", 5,5,300));
        cpu.Add(new Task("task9", 7,7,300));
        cpu.Add(new Task("task10", 9,9,350));*/
        
        // Real dvs.
      cpu.Add(new Task("task1", 4,15,300));
      cpu.Add(new Task("task2", 2,15,300));
      cpu.Add(new Task("task3", 3,10,250));
      cpu.Add(new Task("task4", 4,16,240));
      cpu.Add(new Task("task5", 4,14,250));
      cpu.Add(new Task("task6", 1,10,320));
      cpu.Add(new Task("task7", 1,20,340));
      cpu.Add(new Task("task8", 2,15,200));
      cpu.Add(new Task("task9", 4,18,300));
      cpu.Add(new Task("task10", 5,18,300));
      
      //cpu.Add(new Task("task1", 8,10,50));
      //cpu.Add(new Task("task2", 7,15,100));
      //cpu.Add(new Task("task3", 6,20,230));
      //cpu.Add(new Task("task4", 7,14,350));
        
      cpu.run();
	}
}
