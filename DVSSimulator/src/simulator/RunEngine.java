package simulator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

import cpu.Processor;
import cpu.ProcessorState;

public class RunEngine implements Runnable, Engine {
	
	protected volatile BlockingQueue<Task> _msgQueue = null;
	private volatile Task _runningTask = null;
	private List<Task> _resettedTasks;
	private int _state;
	private Processor proc1 = new Processor();
	private ProcessorState state = new ProcessorState();
	public RunEngine(BlockingQueue<Task> msgQueue){
		_resettedTasks = new ArrayList<Task>();
		_msgQueue = msgQueue;
		_state = State.C0;
	}
	
	
	public BlockingQueue<Task> getQueue(){
		return _msgQueue;
	}

	@Override
	public Task getRunningTask() {
		return _runningTask;
	}
	
	public void Tick() throws InterruptedException{
		Thread.sleep(getState());
	}

	@Override
	public void run() {
		while(true){
			try {
				//System.out.println("RunEngine: Checking queue...");
				Task newTask = _msgQueue.poll();
				if(newTask == null){
					//System.out.println("RunEngine: No new task...");
					if(_runningTask == null){
						//System.out.println("RunEngine: Running task = null...");
						Thread.sleep(50);
					} else {
						Tick();
						System.out.println("RunEngine: Running task = "+_runningTask.getName()+" with seconds left: "+_runningTask.getTicksleft()*getState()/1000);
						if(_runningTask.getTicksleft()>0){
							_runningTask.setTicksleft(_runningTask.getTicksleft()-1);
							_runningTask.setTicksComputed(_runningTask.getTicksComputed()+1);
						} else {
							System.out.println("RunEngine: Running task = "+_runningTask.getName()+" with ticks reached 0");
							//Output power, duration and energy when the task is completed
							_runningTask.resetPeriod();
							_resettedTasks.add(_runningTask);
							_runningTask = null;
						}
					}
				}
				else{
					System.out.println("RunEngine: New task: "+newTask.getName());
					if(newTask.getName().equals("block")){
						_runningTask = null;
					} else {
						if (newTask.getActualCase() == -1) {
							newTask.setActualCase(randInt(newTask.getBestcase(),newTask.getWorstcase()));
							newTask.setTicksleft(newTask.getActualCase());
						}
						
						_runningTask = newTask;
					}  
					
				}
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
		}
	}
	
	public static int randInt(int min, int max) {

	    // NOTE: This will (intentionally) not run as written so that folks
	    // copy-pasting have to think about how to initialize their
	    // Random instance.  Initialization of the Random instance is outside
	    // the main scope of the question, but some decent options are to have
	    // a field that is initialized once and then re-used as needed or to
	    // use ThreadLocalRandom (if using at least Java 1.7).
	    Random rand = new Random();
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    return rand.nextInt((max - min) + 1) + min;
	}

	@Override
	public List<Task> getResettedTasks() {
		List<Task> temp = new ArrayList<Task>();
		temp.addAll(_resettedTasks);
		_resettedTasks.clear();
		for(int i = 0; i < temp.size(); i++){
			System.out.println("RunEngine: Resetted task: " + temp.get(i));
		}
		return temp;
	}

	public int getState() {
		return _state;
	}
	
	public void setState(int state){
		_state = state;
	}
}
