package simulator;

public class Task {
	private String name;
	private int bestcase;
	private int worstcase;
	private int deadline;
	private int actualcase;
	private int period;
	private int ticksleft;
	private int ticksComputed;
	private int timetorun;
	public Task(String name, int bestcase, int worstcase, int period) {
		this.name = name;
		this.bestcase = bestcase;
		this.worstcase = worstcase;
		this.setTicksleft(worstcase);
		this.deadline = period;
		this.period = period;
		this.actualcase = -1;
		this.setTimetorun(0);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public int getBestcase() {
		return bestcase;
	}
	public void setBestcase(int bestcase) {
		this.bestcase = bestcase;
	}
	public int getWorstcase() {
		return worstcase;
	}
	public void setWorstcase(int worstcase) {
		this.worstcase = worstcase;
	}
	public int getDeadline() {
		return deadline;
	}
	public void setDeadline(int deadline) {
		this.deadline = deadline;
	}
	public void resetPeriod() {
		setTimetorun(getDeadline());
		setDeadline(getPeriod() + getDeadline());
		setTicksComputed(0);
		setTicksleft(getActualCase());
	}
	
	public void setActualCase(int actualcase) {
		this.actualcase = actualcase;
	}
	
	public int getActualCase()	{
		return this.actualcase;
	}
	
	public String toString(){
		return this.getName() +" Deadline: "+ this.getDeadline();
	}

	public int getPeriod() {
		return period;
	}

	public int getTicksleft() {
		return ticksleft;
	}

	public void setTicksleft(int ticksleft) {
		this.ticksleft = ticksleft;
	}

	public int getTicksComputed() {
		return ticksComputed;
	}

	public void setTicksComputed(int ticksComputed) {
		this.ticksComputed = ticksComputed;
	}

	public int getTimetorun() {
		return timetorun;
	}

	public void setTimetorun(int timetorun) {
		this.timetorun = timetorun;
	}	
}
