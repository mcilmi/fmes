package simulator;

public class StopWatch { 

    private final long start;
    
    public StopWatch() {
        start = System.currentTimeMillis();
    } 

    public int elapsedTime() {
        long now = System.currentTimeMillis();
        return (int)(now - start)/100;
    }
}
