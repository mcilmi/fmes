package simulator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class CalculateEnergyUsage{
	
	String taskName;
	int duration;
	String state;
	double util;
	public  CalculateEnergyUsage(String taskName, int duration, String state, double util){
		this.util = util; 
		this.taskName = taskName;
		this.duration = duration;
		this.state = state;
	}
}
